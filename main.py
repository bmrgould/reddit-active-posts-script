import praw
import calendar
import pickle
from datetime import datetime
from userInfo import RedditUser

### Currently setup to be used daily.

directorySub = 'metanarchism' # subreddit to post/edit daily - post functions as a directory of active posts.
directorySubTitle = "Active Proposals & Recent Posts"
reportSub = 'Anarchism' # subreddit to post occasional report backs (weekly) that outline currently active posts.
reportSubTitle = "Weekly Report from Meta Sub"
reportUpdater = False # Update report sub's post when daily updates or leave report the same until next report.
sourceSub = 'metanarchism' # subreddit to grab posts from.
sourceCount = 50 # number of posts to grab. low activity subs, so only grabbing 50, can go to 1000. Change as needed.
saveFileName = "transInfo.p" # File that originalDate, dailyPostId, weeklyPostId are saved to. 
reportText = """Hello,

We're trying to get more participation from the main sub over in r/metanarchism to ensure that the sub we have for moderating /r/anarchism actually reflects the community in /r/anarchism. So, we're going to start doing a weekly report back thread of the active proposals and threads over there to raise awareness of that sub and encourage people to participate. If you want to participate in meta just [contact the mods and request entry](https://www.reddit.com/message/compose?to=%2Fr%2Fmetanarchism) . To qualify you just have to:

1)  be an anarchist or libertarian socialist 

2) have comments dating back more than 3 months in /r/anarchism

3) have more than 10 comments on /r/anarchism

4) have positive karma on /r/Anarchism 

5) not be just a "shitposter"

________________

    """

leeway = 14400 # Four Hours
duration = 604800 # 7 days
def dt2ts(dt):
    """Converts a datetime object to UTC timestamp

    naive datetime will be considered UTC.

    """

    return calendar.timegm(dt.utctimetuple())

#Load File Variables
### Days = Days directoryPost has been alive. directoryPostId is the ID of the active post. reportPostId is the ID of the current report post.
originalDate,directoryPostId,reportPostId = pickle.load(open(saveFileName,"rb"))

#Login to Reddit
reddit = praw.Reddit(client_id=RedditUser.client_id, client_secret=RedditUser.client_secret,
                     password=RedditUser.password, user_agent=RedditUser.user_agent,
                     username=RedditUser.username)
print("Logged In")

#Time Variables
now = datetime.now()
nowString = now.strftime("%d/%m/%y")
nowTime = dt2ts(now)
weekOneTime = nowTime - duration # 7 days ago - One Week
weekTwoTime = weekOneTime - duration # 14 days ago - Two Weeks

#Grab recent posts from sourceSub
posts = reddit.subreddit(sourceSub).new(limit=sourceCount)
print("Recent Posts Collected")
#Split posts into Week One, Week Two
weekTwoPosts = [post for post in posts if post.created > weekTwoTime]
weekOnePosts = [post for post in weekTwoPosts if post.created > weekOneTime]
weekTwoPosts = [post for post in weekTwoPosts if post.created < weekOneTime]

#Set Text for Week One Posts
subText = "\n\n **List of Active Proposals and Misc. Posts** (7 days): \n\n"
for post in weekOnePosts:
    if post.link_flair_text is None:
        postFlair = ''
    else:
        postFlair = "*" + post.link_flair_text + "* "
    subText += "* +" + str(post.score) + " " + postFlair + " [" + post.title + "](" + post.url + ")  [" + str(post.num_comments) + "_comments]\n\n"

#Set Text for Week Two Posts
subText += "\n\n **Previous Week's Posts** (14 days): \n\n"
for post in weekTwoPosts:
    if post.link_flair_text is None:
        postFlair = ''
    else:
        postFlair = "*" + post.link_flair_text + "* "
    subText += "* +" + str(post.score) + " " + postFlair + " [" + post.title + "](" + post.url + ")  [" + str(post.num_comments) + "_comments]\n\n"

#Set Text for Date Updated
subText += "\n\nLast Updated: " + nowString
#Set reportText with finished subText
reportText += subText

#if more than duration, new report post, new directory post
if (nowTime - duration + leeway) > originalDate :
    # reset report and directory posts IDs
    if directoryPostId :
        directoryPostId = None
    if reportPostId :
        reportPostId = None
    originalDate = nowTime
    

if reportPostId is None:
    #new post
    reportPostId = reddit.subreddit(reportSub).submit(reportSubTitle, selftext=reportText)
    reportPost = reddit.submission(id=reportPostId)
    print("New Report Post")
else:
    #edit existing if reportUpdater
    if reportUpdater is True:
        reportPost = reddit.submission(id=reportPostId)
        reportPost.edit(reportText)
        print("Updated Report Post")

#daily post
if directoryPostId is None:
    #new post
    directoryPostId = reddit.subreddit(directorySub).submit(directorySubTitle, selftext=subText)
    directoryPost = reddit.submission(id=directoryPostId)
    print("New Directory Post")
else:
    #edit exisiting
    directoryPost = reddit.submission(id=directoryPostId)
    directoryPost.edit(subText)
    print("Updated Directory Post")


#Save Variables to File
pickle.dump([originalDate,directoryPostId,reportPostId], open(saveFileName, "wb"))
